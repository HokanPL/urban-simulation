﻿using AppliacationManagement;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UrbanWorld.Pathfinding
{
    public class PavementNetwork : MonoBehaviour
    {
        [SerializeField] private ApplicationManager mediator;
        
        public NodeOOP[,] nodes { get; private set; }

        public int sizeX { get; private set; } = 0;
        public int sizeZ { get; private set; } = 0;

        public void CreateNetwork()
        {
            sizeX = 2 + 2 * mediator.SizeX;
            sizeZ = 2 + 2 * mediator.SizeZ;

            nodes = new NodeOOP[sizeX, sizeZ];

            for (int i = 0; i < sizeX; i++)
                for (int j = 0; j < sizeZ; j++)
                {
                    nodes[i, j] = new NodeOOP();
                    nodes[i, j].position = new Vector3(0, -1, 0);
                    nodes[i, j].gCost = 0;
                    nodes[i, j].hCost = 0;
                }

            List<Vector3> positions = new List<Vector3>();

            var placeables = FindObjectsOfType<Placeable>();
            foreach (var p in placeables)
                positions.AddRange(p.GetWalkableCoords());


            foreach (Vector3 p in positions)
            {
                int x = Mathf.RoundToInt(p.x / 0.5f + 1);
                int z = Mathf.RoundToInt(p.z / 0.5f + 1);
                nodes[x, z] = new NodeOOP
                {
                    position = p,
                    gCost = 0,
                    hCost = 0
                };
            }

        }
        public void ClearNetwork()
        {
            nodes = new NodeOOP[0, 0];
        }

        public NodeOOP GetRandomNode(bool hasToBeWalkable = true)
        {
            if (!hasToBeWalkable)
                return nodes[Random.Range(0, sizeX), Random.Range(0, sizeZ)];
            else
            {
                var nodesToSelect = new List<NodeOOP>();
                for (int i = 0; i < sizeX; i++)
                {
                    for (int j = 0; j < sizeZ; j++)
                    {
                        if (nodes[i, j].walkable)
                            nodesToSelect.Add(nodes[i, j]);
                    }
                }
                return nodesToSelect[Random.Range(0, nodesToSelect.Count)];
            }
        }

        public NodeOOP GetNodeByPosition(Vector3 position)
        {
            NodeOOP result = null;
            float minDistance = float.MaxValue;
            for (int i = 0; i < nodes.GetLength(0); i++)
            {
                for (int j = 0; j < nodes.GetLength(1); j++)
                {
                    float currentDistance = Vector3.Distance(position, nodes[i, j].position);
                    if (currentDistance < minDistance)
                    {
                        result = nodes[i, j];
                        minDistance = currentDistance;
                    }
                }
            }

            return result;
        }

        public List<NodeOOP> GetNeighbours(NodeOOP node)
        {
            var coords = GetNodeCoords(node);
            List<NodeOOP> result = new List<NodeOOP>();
            
            //if x is -1, y is also -1, so no need to check both
            if (coords.x < 0)
                return result;

            int northZ = coords.y + 1;
            int southZ = coords.y - 1;
            int eastX = coords.x + 1;
            int westX = coords.x - 1;

            if (northZ < sizeZ)
                result.Add(nodes[coords.x, northZ]);

            if (southZ >= 0)
                result.Add(nodes[coords.x, southZ]);

            if (eastX < sizeX)
                result.Add(nodes[eastX, coords.y]);

            if (westX >= 0)
                result.Add(nodes[westX, coords.y]);

            return result;
        }

        public uint GetDistance(NodeOOP nodeA, NodeOOP nodeB)
        {
            var coordsA = GetNodeCoords(nodeA);
            var coordsB = GetNodeCoords(nodeB);

            return (uint)(Mathf.Abs(coordsB.x - coordsA.x) + Mathf.Abs(coordsB.y - coordsA.y));
        }

        private Vector2Int GetNodeCoords(NodeOOP node)
        {
            for (int i = 0; i < nodes.GetLength(0); i++)
                for (int j = 0; j < nodes.GetLength(1); j++)
                    if (nodes[i, j] == node)
                        return new Vector2Int(i, j);

            return new Vector2Int(-1, -1);
        }
    }

    public class NodeOOP
    {
        public Vector3 position;
        public bool walkable => position.y >= 0;
        public uint gCost;
        public uint hCost;
        public uint fCost => gCost + hCost;

        public NodeOOP parent;
    }
}
