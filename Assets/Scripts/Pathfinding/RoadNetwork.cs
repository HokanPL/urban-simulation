﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UrbanWorld.Pathfinding
{
    public class RoadNetwork : MonoBehaviour
    {
        //przerob tak, by traffic routy mialy 2x vector3 a nie connectory
        public HashSet<TrafficRoute> trafficRoutes = new HashSet<TrafficRoute>();

        private Connector[] connectors = new Connector[0];

        public void CreateNetwork()
        {
            //select all connectors
            connectors = FindObjectsOfType<Connector>(false);

            //abort if none detected
            if (connectors.Length == 0)
                return;

            //divide connectors into two groups based on their type IN/OUT
            var connectorsIn = connectors.Select(c => c).Where(c => c.type == ConnectorType.In).ToList();
            var connectorsOut = connectors.Select(c => c).Where(c => c.type == ConnectorType.Out).ToList();

            //initialize empty collection of unique routes
            trafficRoutes.Clear();

            //collect routes from road corners and add them to collection
            foreach (var conIn in connectorsIn)
            {
                foreach (var r in conIn.paths)
                {
                    TrafficRoute route = new TrafficRoute();
                    route.startPosition = conIn.transform.position;
                    route.endPosition = r.connectorOut.transform.position;
                    trafficRoutes.Add(route);
                }
            }
            
            //collect routes between any type of intersections and add them to collection
            foreach (var conOut in connectorsOut)
            {
                //you can connect only 'ins' and 'outs' with the same movement direction
                var inCandidates = connectorsIn.Select(c => c).Where(c => c.moveDirection == conOut.moveDirection).ToArray();
                Connector chosenIn = null;

                switch (conOut.moveDirection)
                {
                    case WorldDirection.south:
                        chosenIn = inCandidates.Select(c => c)
                            .Where(c => c.transform.position.x == conOut.transform.position.x && c.transform.position.z < conOut.transform.position.z && conOut.transform.position.z - c.transform.position.z < 0.5f)
                            .OrderBy(c => conOut.transform.position.z - c.transform.position.z).FirstOrDefault();
                        break;

                    case WorldDirection.north:
                        chosenIn = inCandidates.Select(c => c)
                            .Where(c => c.transform.position.x == conOut.transform.position.x && c.transform.position.z > conOut.transform.position.z && c.transform.position.z - conOut.transform.position.z < 0.5f)
                            .OrderBy(c => c.transform.position.z - conOut.transform.position.z).FirstOrDefault();
                        break;

                    case WorldDirection.west:
                        chosenIn = inCandidates.Select(c => c)
                            .Where(c => c.transform.position.z == conOut.transform.position.z && c.transform.position.x < conOut.transform.position.x && conOut.transform.position.x - c.transform.position.x < 0.5f)
                            .OrderBy(c => conOut.transform.position.x - c.transform.position.x).FirstOrDefault();
                        break;
                    case WorldDirection.east:
                        chosenIn = inCandidates.Select(c => c)
                            .Where(c => c.transform.position.z == conOut.transform.position.z && c.transform.position.x > conOut.transform.position.x && c.transform.position.x - conOut.transform.position.x < 0.5f)
                            .OrderBy(c => c.transform.position.x - conOut.transform.position.x).FirstOrDefault();
                        break;
                }

                if (chosenIn is null)
                    continue;

                TrafficRoute route = new TrafficRoute();
                //in and out have to be swapped in connections between neighbour squares
                route.startPosition = conOut.transform.position;
                route.endPosition = chosenIn.transform.position;
                trafficRoutes.Add(route);

                //each in can have only one out between separate nodes;
                //many-to-many exists only in intersections, but it was done in previous step
                connectorsIn.Remove(chosenIn);
            }
        }
        public void ClearNetwork()
        {
            trafficRoutes.Clear();
        }

        public Vector3 GetRandomRouteInPosition()
        {
            var arrayOfRoutes = trafficRoutes.ToArray();
            var amount = arrayOfRoutes.Length;
            
            return arrayOfRoutes[Random.Range(0, amount)].startPosition;
        }

        public Vector3 GetRandomPosition()
        {
            var routes = trafficRoutes.ToArray();
            return routes[Random.Range(0, routes.Length)].endPosition;
        }

        public Vector3 GetRandomAchieveablePosition(Vector3 position)
        {
            var routes = GetTrafficRoutesByStartPosition(position);
            return routes[Random.Range(0, routes.Length)].endPosition;
        }

        private TrafficRoute[] GetTrafficRoutesByStartPosition(Vector3 position)
        {
            //var theClosest = trafficRoutes.OrderBy(r => Vector3.Distance(r.startPosition, position)).First();
            return trafficRoutes.Select(r => r).Where(r => Vector3.Distance(r.startPosition, position) < 0.05f).ToArray();
        }

        //nested class
        public class TrafficRoute
        {
            public Vector3 startPosition;
            public Vector3 endPosition;
        }
    }
}