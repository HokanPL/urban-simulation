using UnityEngine;

namespace UrbanWorld.Pathfinding
{
    public class RoadPathfinding : MonoBehaviour
    {
        public RoadNetwork roadNetwork;

        public static RoadPathfinding Instance { get; private set; }

        private void Awake()
        {
            if (Instance != null && Instance != this)
                Destroy(gameObject);
            else
                Instance = this;
        }

        public Vector3 GetNextTarget(Vector3 currentPosition)
        {
            return roadNetwork.GetRandomAchieveablePosition(currentPosition);
        }
    }
}