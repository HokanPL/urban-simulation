using AppliacationManagement;
using System.Collections;
using System.IO;
using UnityEngine;

public class PerformanceMonitor : MonoBehaviour
{
    [SerializeField] private ApplicationManager mediator;
    [SerializeField] [Min(5f)] private float simulationTime;
    [SerializeField] [Min(0.1f)] private float simulationStep;
    [SerializeField] private string measureDataDirectoryPath;

    private void Awake()
    {
        if (measureDataDirectoryPath.Equals(""))
            measureDataDirectoryPath = $"{Application.persistentDataPath}";
    }
    public float InvokeMonitoring()
    {
        StartCoroutine(Measure(simulationTime));
        return simulationTime;
    }

    private IEnumerator Measure(float measureTime)
    {
        string simulationMode = (mediator.SimulationMode == 1) ? "DOTS" : $"OOP";
        string combiningMode = mediator.CombiningMode.ToString();
        string resolution = $"{Screen.width}x{Screen.height}";
        string simulationName = $"{mediator.SimulationName}";
        string path;
        if (!mediator.IsCameraDisabled)
            if (mediator.SimulationMode == 1) //if DOTS
                path = $"{measureDataDirectoryPath}/{simulationName}_{simulationMode}_{resolution}_CameraEnabled.txt";
            else //if OOP
                path = $"{measureDataDirectoryPath}/{simulationName}_{simulationMode}_{combiningMode}_{resolution}_CameraEnabled.txt";
        else //no matter if OOP or DOTS, combining mode is not used
            path = $"{measureDataDirectoryPath}/{simulationName}_{simulationMode}_{resolution}_CameraDisabled.txt";

        using (StreamWriter writer = File.CreateText(path))
        {
            writer.WriteLine($"#Performance in time for level {mediator.SizeX}x{mediator.SizeZ}");
            writer.WriteLine($"#Simulation name: {simulationName}");
            writer.WriteLine($"#Simulation mode: {simulationMode}");
            writer.WriteLine($"#Camera enabled: {!mediator.IsCameraDisabled}");
            if (!mediator.IsCameraDisabled)
                writer.WriteLine($"#Mesh combining mode: {combiningMode}");
            writer.WriteLine($"#Screen resolution (in pixels): {resolution}");
            writer.WriteLine("#");
            writer.WriteLine("#t[s]\tv[FPS]");
            writer.WriteLine("#");

            float timeCurrent = 0;
            float deltaTime = 0;
            int fpsCount = 0;
            while (timeCurrent < measureTime)
            {
                timeCurrent += Time.deltaTime;
                deltaTime += Time.deltaTime;
                fpsCount++;

                if (deltaTime >= simulationStep)
                {
                    string record = $"{timeCurrent}\t{fpsCount / deltaTime}";
                    writer.WriteLine(record.Replace(',', '.'));
                    deltaTime = 0;
                    fpsCount = 0;
                }
                yield return null;
            }
        }
    }
}