using AppliacationManagement;
using UrbanWorld.Generators;
using SaveLoadManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace UIManagement
{
    public static class PanelsKeys
    {
        public const string HOME = "Home";
        public const string GENERATOR = "Generator";
        public const string SAVELOAD = "SaveLoad";
        public const string STARTSIMULATION = "StartSimulation";
    }
    public class MenuNavigation : MonoBehaviour
    {
        [Header("Application modules")]
        [SerializeField] LevelSaveLoadData saveLoadFeatures;
        [SerializeField] LevelGenerator generatorFeatures;
        [SerializeField] LevelVisualizer visualizerFeatures;
        [SerializeField] PerformanceMonitor performanceMonitorFeatures;
        [SerializeField] ApplicationManager mediatorFeatures;

        [Header("Home panel")]
        [SerializeField] RectTransform home_Panel;
        [SerializeField] Button openLevelGenerator_Button;
        [SerializeField] Button openSavesManager_Button;
        [SerializeField] Button exitApplication_Button;

        [Header("Save-Load panel")]
        [SerializeField] RectTransform saveLoad_Panel;
        [SerializeField] RectTransform savesHolder;
        [SerializeField] Button loadSavedFile_Button;
        [SerializeField] Button deleteSavedFile_Button;
        [SerializeField] Button backToHomePanelFromSaveLoadButton_Button;
        [SerializeField] Image preview_Image;

        [Header("Generator panel")]
        [SerializeField] RectTransform generator_Panel;
        [SerializeField] InputField levelSizeX_Input;
        [SerializeField] InputField levelSizeZ_Input;
        [SerializeField] InputField levelName_Input;
        [SerializeField] Button generate_Button;
        [SerializeField] Button backToHomePanelFromGenerator_Button;

        [Header("Start simulation panel")]
        [SerializeField] RectTransform startSimulation_Panel;
        [SerializeField] Button startSimulation_Button;
        [SerializeField] Button backToHomePanelFromStartSimulation_Button;
        [SerializeField] Dropdown simulationMode_DropDown;
        [SerializeField] Dropdown meshesCombining_DropDown;
        [SerializeField] Toggle disableCamera_Toggle;

        [Header("Prefabs")]
        [SerializeField] Button savePrefab;

        private string saveSelected = "";
        private bool disableCamera = false; //camera should be rendering
        private int simulationMode = 0; //0 stands for OOP, 1 for DOTS
        private LevelVisualizer.CombiningMode combiningMode = LevelVisualizer.CombiningMode.noneCombining;
        


        private void Start()
        {
            mediatorFeatures.OnNodesPushed += visualizerFeatures.ShowUrbanWorld;

            openLevelGenerator_Button.onClick.AddListener(() =>
            {
                GoToPanel(PanelsKeys.GENERATOR);
            });
            openSavesManager_Button.onClick.AddListener(() =>
            {
                GoToPanel(PanelsKeys.SAVELOAD);
                saveSelected = "";
                loadSavedFile_Button.interactable = false;
                deleteSavedFile_Button.interactable = false;
                SerializeAllSaves();
            });
            exitApplication_Button.onClick.AddListener(() =>
            {
                Application.Quit();
            });

            loadSavedFile_Button.onClick.AddListener(() =>
            {
                if (saveSelected.Equals(""))
                    return;
                saveLoadFeatures.Load(saveSelected);
                GoToPanel(PanelsKeys.STARTSIMULATION);
            });
            deleteSavedFile_Button.onClick.AddListener(() =>
            {
                if (saveSelected.Equals(""))
                    return;

                loadSavedFile_Button.interactable = false;
                deleteSavedFile_Button.interactable = false;

                saveLoadFeatures.Delete(saveSelected);
                SerializeAllSaves();
            });
            backToHomePanelFromSaveLoadButton_Button.onClick.AddListener(() =>
            {
                GoToPanel(PanelsKeys.HOME);
            });
            generate_Button.onClick.AddListener(() =>
            {
                saveSelected = levelName_Input.text;
                generatorFeatures.Generate(saveSelected, uint.Parse(levelSizeX_Input.text), uint.Parse(levelSizeZ_Input.text));
                GoToPanel(PanelsKeys.STARTSIMULATION);
            });
            backToHomePanelFromGenerator_Button.onClick.AddListener(() =>
            {
                GoToPanel(PanelsKeys.HOME);
            });
            startSimulation_Button.onClick.AddListener(() =>
            {
                visualizerFeatures.StartSimulation(
                    simulationMode, combiningMode, disableCamera,
                    () => {
                        GoToPanel(PanelsKeys.STARTSIMULATION);
                        saveLoadFeatures.Save(saveSelected);
                        },
                    performanceMonitorFeatures);

                HideAllPanels();
            });

            backToHomePanelFromStartSimulation_Button.onClick.AddListener(() =>
            {
                generatorFeatures.ClearGeneratedData();
                GoToPanel(PanelsKeys.HOME);
            });



            levelSizeX_Input.onValueChanged.AddListener((value) =>
            {
                SetGenerateButtonInteractable();
            });
            levelSizeX_Input.onEndEdit.AddListener((value) =>
            {
                if (!value.Equals("") && int.Parse(value) < 6)
                    levelSizeX_Input.text = "6";
            });

            levelSizeZ_Input.onValueChanged.AddListener((value) =>
            {
                SetGenerateButtonInteractable();
            });
            levelSizeZ_Input.onEndEdit.AddListener((value) =>
            {
                if (!value.Equals("") && int.Parse(value) < 6)
                    levelSizeZ_Input.text = "6";
            });

            levelName_Input.onValueChanged.AddListener((value) =>
            {
                SetGenerateButtonInteractable();
            });

            simulationMode_DropDown.onValueChanged.AddListener((value) =>
            {
                simulationMode = value;
                if (value == 0)
                {
                    meshesCombining_DropDown.interactable = !disableCamera_Toggle.isOn;
                }
                else
                {
                    meshesCombining_DropDown.interactable = false;
                    combiningMode = LevelVisualizer.CombiningMode.noneCombining;
                    meshesCombining_DropDown.value = 0;
                }
            });

            meshesCombining_DropDown.onValueChanged.AddListener((value) =>
            {
                switch(value)
                {
                    case 0: combiningMode = LevelVisualizer.CombiningMode.noneCombining; break;
                    case 1: combiningMode = LevelVisualizer.CombiningMode.indexFormat16; break;
                    case 2: combiningMode = LevelVisualizer.CombiningMode.indexFormat32; break;
                }
            });

            disableCamera_Toggle.onValueChanged.AddListener((value) =>
            {
                disableCamera = value;
                if (value)
                {
                    meshesCombining_DropDown.interactable = false;
                    meshesCombining_DropDown.value = 0;
                    combiningMode = LevelVisualizer.CombiningMode.noneCombining;
                }
                else
                {
                    meshesCombining_DropDown.interactable = (simulationMode_DropDown.value == 0);
                }
            });

            GoToPanel(PanelsKeys.HOME);
        }

        private void SetGenerateButtonInteractable()
        {
            generate_Button.interactable = !(levelSizeX_Input.text.Equals("") || levelSizeZ_Input.text.Equals("") || levelName_Input.text.Equals(""));
        }

        private void GoToPanel(string panelName)
        {
            HideAllPanels();

            switch (panelName)
            {
                case PanelsKeys.HOME:
                    home_Panel.gameObject.SetActive(true);
                    loadSavedFile_Button.interactable = false;
                    deleteSavedFile_Button.interactable = false;
                    generate_Button.interactable = false;
                    preview_Image.sprite = null;
                    levelSizeX_Input.text = "";
                    levelSizeZ_Input.text = "";
                    levelName_Input.text = "";
                    saveSelected = "";
                    disableCamera = false;
                    simulationMode = 0;
                    simulationMode_DropDown.value = 0;
                    combiningMode = LevelVisualizer.CombiningMode.noneCombining;
                    meshesCombining_DropDown.value = 0;
                    break;

                case PanelsKeys.SAVELOAD:
                    saveLoad_Panel.gameObject.SetActive(true);
                    break;

                case PanelsKeys.GENERATOR:
                    generator_Panel.gameObject.SetActive(true);
                    break;

                case PanelsKeys.STARTSIMULATION:
                    saveLoadFeatures.Load(saveSelected); //- it is loaded already
                    visualizerFeatures.ShowUrbanWorld(); //- not here because an event is invoked
                    startSimulation_Panel.gameObject.SetActive(true);
                    break;
                default:
                    Debug.LogError($"Panel \'{panelName}\' is not recognized.");
                    break;
            }
        }
        private void HideAllPanels()
        {
            home_Panel.gameObject.SetActive(false);
            saveLoad_Panel.gameObject.SetActive(false);
            generator_Panel.gameObject.SetActive(false);
            startSimulation_Panel.gameObject.SetActive(false);
        }

        private void SerializeAllSaves()
        {
            //Clear obsolete list
            foreach (Transform child in savesHolder.transform)
            {
                Destroy(child.gameObject);
            }
            //Get all general info about files
            var savedFiles = saveLoadFeatures.ShowAllSavedLevels();

            //Show all of them and add listener - selection
            for (int i = savedFiles.Length - 1; i >= 0; i--)
            {
                Button obj = Instantiate(savePrefab, savesHolder);
                obj.GetComponentInChildren<Text>().text = savedFiles[i];
                string str = savedFiles[i];
                obj.onClick.AddListener(() => {
                    saveSelected = str;
                    loadSavedFile_Button.interactable = true;
                    deleteSavedFile_Button.interactable = true;
                });
            }
        }
        public void DeleteSelectedSave()
        {
            saveLoadFeatures.Delete(saveSelected);
            SerializeAllSaves();
        }
    }
}