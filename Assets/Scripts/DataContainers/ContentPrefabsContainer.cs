
using UnityEngine;
using UrbanWorld;

public class ContentPrefabsContainer : MonoBehaviour
{
    public Building[] placeableBuildings;
    public Road[] placeableRoads;
}
