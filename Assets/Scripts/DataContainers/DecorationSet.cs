﻿using System;
using UnityEngine;

[Serializable]
public class DecorationSet
{
    [Range(0, 100f)]
    [SerializeField] private float chanceToShow;
    [SerializeField] private GameObject[] optionalDecorations;

    public void TryShowRandomDecoration()
    {
        if (optionalDecorations is null)
            return;

        int length = optionalDecorations.Length;

        for (int i = 0; i < length; i++)
            optionalDecorations[i].SetActive(false);

        if (UnityEngine.Random.Range(0, 100f) < 100f-chanceToShow)
            return;

        optionalDecorations[UnityEngine.Random.Range(0, length)].SetActive(true);
    }
}