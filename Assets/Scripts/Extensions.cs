﻿using System;

namespace Unity.Mathematics.Extensions
{
    public static class Float3Extensions
    {
        public static float3 MoveTowards(float3 current, float3 target, float maxDistanceDelta)
        {
            float deltaX = target.x - current.x;
            float deltaY = target.y - current.y;
            float deltaZ = target.z - current.z;
            float sqdist = deltaX * deltaX + deltaY * deltaY + deltaZ * deltaZ;
            var dist = (float)Math.Sqrt(sqdist);
            if (dist == 0 || dist <= maxDistanceDelta)
                return target;
            return new float3(current.x + deltaX / dist * maxDistanceDelta,
                current.y + deltaY / dist * maxDistanceDelta,
                current.z + deltaZ / dist * maxDistanceDelta);
        }
    }
}
