﻿using System.IO;

namespace SaveLoadManagement
{
    public interface ILoadSaveData
    {
        bool Load(string path, string filename);
        bool Save(string path, string filename);
        bool Delete(string path, string filename);

        string[] ShowAllSaves(string path);
    }
}