﻿using UnityEngine;
using UrbanWorld;
using System.IO;
using System.Linq;
using System.Collections.Generic;

namespace SaveLoadManagement
{
    public class LoadSaveLevelDataJson : IBindLevelData
    {
        private Cell[,] levelData;
        public void BindData(Cell[,] nodes) => levelData = nodes;

        public Cell[,] RetrieveData() => levelData;

        public bool Load(string path, string filename)
        {
            try
            {
                string filepath = $"{path}/{filename}.usd";
                string json = File.ReadAllText(filepath);
                LevelData dataLoaded = JsonUtility.FromJson<LevelData>(json);
                levelData = dataLoaded.GetNodes();

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Save(string path, string filename)
        {
            try
            {
                string filepath = $"{path}/{filename}.usd"; //Urban Simulation Data
                LevelData dataToSave = new LevelData(levelData);
                string json = JsonUtility.ToJson(dataToSave);
                File.WriteAllText(filepath, json);

                return true;
            }
            catch
            {
                return false;
            }
        }

        public bool Delete(string path, string filename)
        {
            try
            {
                string filepath = $"{path}/{filename}.usd"; //Urban Simulation Data
                File.Delete(filepath);
                return true;
            }
            catch
            {
                return false;
            }
        }

        public string[] ShowAllSaves(string path)
        {
            var info = new DirectoryInfo(path);
            var data = info.GetFiles().Select(f => f).Where(f => f.Name.EndsWith(".usd")).OrderBy(f => f.LastAccessTimeUtc).Select(n => n.Name.Remove(n.Name.Length - 4, 4)).ToArray();

            List<string> result = new List<string>();
            foreach (var d in data)
            {
                if (Load(path, d))
                    result.Add(d);
            }
            return result.ToArray();
        }
    }
}