using AppliacationManagement;
using UnityEngine;
using UrbanWorld;

namespace SaveLoadManagement
{
    public class LevelSaveLoadData : MonoBehaviour
    {
        [SerializeField] private ApplicationManager mediator;

        [SerializeField] private string saveloadDirectoryPath;

        private IBindLevelData dataLoadSave;

        private Cell[,] levelData;

        private void Awake()
        {
            levelData = new Cell[0, 0];

            if (saveloadDirectoryPath.Equals(""))
                saveloadDirectoryPath = $"{Application.persistentDataPath}";
            dataLoadSave = new LoadSaveLevelDataJson();
        }

        public void Load(string filename)
        {
            if (dataLoadSave.Load(saveloadDirectoryPath, filename))
            {
                mediator.PushCells(dataLoadSave.RetrieveData(), filename, true);
            }
        }

        public void Save(string filename)
        {
            if (mediator.IsSimulationLoadedFromFile)
                return;

            levelData = mediator.PullCells();

            if (levelData.Length == 0 || filename.Equals(""))
            {
                Debug.LogWarning("There is no data to save. Aborted.");
                return;
            }
            dataLoadSave.BindData(levelData);
            dataLoadSave.Save(saveloadDirectoryPath, filename);
        }

        public void Delete(string filename)
        {
            if (filename.Equals(""))
            {
                Debug.LogWarning("There is no file to delete. Aborted.");
                return;
            }
            dataLoadSave.Delete(saveloadDirectoryPath, filename);
        }

        public string[] ShowAllSavedLevels()
        {
            return dataLoadSave.ShowAllSaves(saveloadDirectoryPath);
        }
    }

    public interface IBindLevelData : ILoadSaveData
    {
        void BindData(Cell[,] nodes);
        Cell[,] RetrieveData();
    }
}