using AppliacationManagement;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Unity.Entities;
using UnityEngine;
using UnityEngine.Events;
using UrbanWorld.Pathfinding;
using UrbanWorld.Simulation;

namespace UrbanWorld.Generators
{
    public class LevelVisualizer : MonoBehaviour
    {
        [SerializeField] private ApplicationManager mediator;

        [SerializeField] private CameraController mainCameraHolder;

        [SerializeField] private ContentPrefabsContainer prefabsContainer;
        [SerializeField] private NetworkGenerator networkGenerator;
        [SerializeField] private GameObjectsSpawner spawner;

        [SerializeField] private Material commonMaterial;

        [SerializeField] private GameObject parentForCombinedMeshes;
        [SerializeField] private GameObject parentForSpawnedPlaceables;

        private Dictionary<string, Placeable> placeablesCatalog;

        private void Start()
        {
            var placeableBuildings = prefabsContainer.placeableBuildings;
            var placeableRoads = prefabsContainer.placeableRoads;

            placeablesCatalog = new Dictionary<string, Placeable>();

            for (int i = 0; i < placeableBuildings.Length; i++)
            {
                if (placeableBuildings[i] == null) continue;
                placeablesCatalog[placeableBuildings[i].name] = placeableBuildings[i];
            }
            for (int i = 0; i < placeableRoads.Length; i++)
            {
                if (placeableRoads[i] == null) continue;
                placeablesCatalog[placeableRoads[i].name] = placeableRoads[i];
            }

            mediator.OnNodesPushed += SetCameraPosition;

            SetupDOTSSystems();
        }

        public void ShowUrbanWorld()
        {
            HideUrbanWorld();
            var cells = mediator.PullCells();

            foreach (Cell node in cells)
            {
                if (placeablesCatalog.ContainsKey(node.placeableName))
                {
                    GameObject obj = Instantiate(
                        placeablesCatalog[node.placeableName].gameObject,
                        new Vector3(node.nodePositionX, 0, node.nodePositionZ),
                        placeablesCatalog[node.placeableName].gameObject.transform.rotation,
                        parentForSpawnedPlaceables.transform
                        );

                    if (obj.GetComponent<Building>())
                        obj.GetComponent<Building>().RandomizeDecorations();
                }
            }
        }

        private void SetCameraPosition()
        {
            var cells = mediator.PullCells();
            Vector2 positionToSet = new Vector2(cells.GetLength(0) * 0.5f, cells.GetLength(1) * 0.5f);
            float radius = 1.5f * Mathf.Max(positionToSet.x, positionToSet.y);
            mainCameraHolder.SetPosition(positionToSet, radius);
        }

        public void HideUrbanWorld()
        {
            var objsCombined = parentForCombinedMeshes.GetComponentsInChildren<Transform>();
            for (int i = objsCombined.Length; i > 1;)
            {
                Destroy(objsCombined[--i].gameObject);
            }
            var objsPlaceables = parentForSpawnedPlaceables.GetComponentsInChildren<Transform>();
            for (int i = objsPlaceables.Length; i > 1;)
            {
                Destroy(objsPlaceables[--i].gameObject);
            }

        }
        private IEnumerator CombineWorldMeshes(GameObject parent)
        {
            yield return null;

            List<MeshFilter> meshFilters = parent.GetComponentsInChildren<MeshFilter>().ToList();

            while (meshFilters.Count != 0)
            {
                int firstId = meshFilters.Count - 1;

                if (firstId < 0) break;

                int totalVertexCount = 0;

                for (int id = firstId; id >= -1; id--)
                {
                    if (id >= 0 && totalVertexCount + meshFilters[id].mesh.vertexCount < ushort.MaxValue)
                    {
                        totalVertexCount += meshFilters[id].mesh.vertexCount;
                        continue;
                    }

                    int size = firstId - id;
                    CombineInstance[] combine = new CombineInstance[size];

                    var selection = meshFilters.GetRange(id + 1, size);

                    for (int c = 0; c < combine.Length; c++)
                    {
                        combine[c].mesh = selection[c].sharedMesh;
                        combine[c].transform = selection[c].transform.localToWorldMatrix;
                        Destroy(selection[c].gameObject);
                    }

                    var obj = new GameObject("[combined mesh]");
                    var meshComp = obj.AddComponent<MeshFilter>();
                    var meshRend = obj.AddComponent<MeshRenderer>();
                    meshComp.mesh = new Mesh();
                    meshComp.mesh.CombineMeshes(combine);
                    meshRend.material = commonMaterial;

                    obj.transform.SetParent(parentForCombinedMeshes.transform);

                    meshFilters.RemoveRange(id + 1, size);

                    break;
                }
            }
        }
        private IEnumerator CombineWorldMeshes32(GameObject parent)
        {
            yield return null;

            List<MeshFilter> meshFilters = parent.GetComponentsInChildren<MeshFilter>().ToList();

            while (meshFilters.Count != 0)
            {
                int firstId = meshFilters.Count - 1;

                if (firstId < 0) break;

                uint totalVertexCount = 0;

                for (int id = firstId; id >= -1; id--)
                {
                    if (id >= 0 && totalVertexCount + (uint)meshFilters[id].mesh.vertexCount < uint.MaxValue)
                    {
                        totalVertexCount += (uint)meshFilters[id].mesh.vertexCount;
                        continue;
                    }

                    int size = firstId - id;
                    CombineInstance[] combine = new CombineInstance[size];

                    var selection = meshFilters.GetRange(id + 1, size);

                    for (int c = 0; c < combine.Length; c++)
                    {
                        combine[c].mesh = selection[c].sharedMesh;
                        combine[c].transform = selection[c].transform.localToWorldMatrix;
                        selection[c].gameObject.SetActive(false);
                    }

                    var obj = new GameObject("[combined mesh 32]");
                    var meshComp = obj.AddComponent<MeshFilter>();
                    var meshRend = obj.AddComponent<MeshRenderer>();
                    meshComp.mesh = new Mesh();
                    meshComp.mesh.indexFormat = UnityEngine.Rendering.IndexFormat.UInt32;
                    meshComp.mesh.CombineMeshes(combine);
                    meshRend.material = commonMaterial;

                    obj.transform.SetParent(parentForCombinedMeshes.transform);

                    meshFilters.RemoveRange(id + 1, size);

                    break;
                }
            }
        }

        public void StartSimulation(int simulationMode, CombiningMode combiningMode, bool disableCamera, UnityAction onComplete, PerformanceMonitor performanceMonitor)
        {
            StartCoroutine(SimulationProgress(simulationMode, combiningMode, disableCamera, onComplete, performanceMonitor));
        }

        public enum CombiningMode { noneCombining, indexFormat16, indexFormat32}

        
        private IEnumerator SimulationProgress(int simulationMode, CombiningMode combiningMode, bool disableCamera, UnityAction onComplete, PerformanceMonitor performanceMonitor)
        {
            mediator.SetSimulationMode(simulationMode);
            mediator.SetCombiningMode(combiningMode);
            mediator.SetCameraMode(disableCamera);
            mainCameraHolder.myCamera.enabled = !mediator.IsCameraDisabled;

            switch (combiningMode)
            {
                case CombiningMode.indexFormat16:
                    yield return StartCoroutine(CombineWorldMeshes(parentForSpawnedPlaceables));
                    break;
                case CombiningMode.indexFormat32:
                    yield return StartCoroutine(CombineWorldMeshes32(parentForSpawnedPlaceables));
                    break;
                default:
                    break;
            }
            networkGenerator.Generate();
            switch (simulationMode)
            {
                case 0:
                    
                    spawner.StartSpawning();
                    yield return null;
                    var meshes = FindObjectsOfType<MeshFilter>(false);
                    var simulationTime = performanceMonitor.InvokeMonitoring();
                    yield return new WaitForSeconds(simulationTime);
                    mainCameraHolder.myCamera.enabled = true;
                    spawner.StopSpawning();
                    
                    break;
                case 1:
                    var placeables = FindObjectsOfType<Placeable>();
                    int amount = placeables.Length;
                    foreach (var p in placeables)
                        p.gameObject.AddComponent<ConvertToEntity>();

                    yield return null;

                    EnableDOTSSystems();
                    yield return null;
                    
                    var spawnerSystem = World.DefaultGameObjectInjectionWorld.GetExistingSystem<SpawnerSystem>();

                    var simulationTim = performanceMonitor.InvokeMonitoring();

                    yield return new WaitForSeconds(simulationTim);

                    DisableDOTSSystems();

                    mainCameraHolder.myCamera.enabled = true;
                    break;
            }

            networkGenerator.Clear();

            HideUrbanWorld();

            onComplete.Invoke();
        }

        private void SetupDOTSSystems()
        {
            World world = World.DefaultGameObjectInjectionWorld;
            var spawnerSystem = world.CreateSystem<SpawnerSystem>();
            var pavementPathfindingSystem = world.CreateSystem<PavementPathfindingSystem>();
            var citizenMovementSystem = world.CreateSystem<CitizenMovementSystem>();
            var vehicleMovementSystem = world.CreateSystem<VehicleMovementSystem>();

            var simulationSystemGroup = world.GetExistingSystem<SimulationSystemGroup>();

            simulationSystemGroup.AddSystemToUpdateList(spawnerSystem);
            simulationSystemGroup.AddSystemToUpdateList(pavementPathfindingSystem);
            simulationSystemGroup.AddSystemToUpdateList(citizenMovementSystem);
            simulationSystemGroup.AddSystemToUpdateList(vehicleMovementSystem);

            spawnerSystem.Enabled = false;
            pavementPathfindingSystem.Enabled = false;
            citizenMovementSystem.Enabled = false;
            vehicleMovementSystem.Enabled = false;
        }

        private void EnableDOTSSystems()
        {
            World world = World.DefaultGameObjectInjectionWorld;
            //var randomSystem = world.GetExistingSystem<RandomSystem>();
            var spawnerSystem = world.GetExistingSystem<SpawnerSystem>();
            var pavementPathfindingSystem = world.GetExistingSystem<PavementPathfindingSystem>();
            var citizenMovementSystem = world.GetExistingSystem<CitizenMovementSystem>();
            var vehicleMovementSystem = world.GetExistingSystem<VehicleMovementSystem>();

            var pulledNodes = networkGenerator.PullNodes();
            var pulledVehicleStartLocations = networkGenerator.PullVehicleStartLocationsAsFloat3s();
            var citizenStartLocations = networkGenerator.PullCitizenStartLocationsAsFloat3s();
            var pulledRoadConnections = networkGenerator.PullRoutes();

            spawnerSystem.vehicleStartLocations = pulledVehicleStartLocations;
            spawnerSystem.citizenStartLocations = citizenStartLocations;
            spawnerSystem.nodesOOP = pulledNodes;
            spawnerSystem.BindCitizenPrefabs(spawner.citizensPrefabs);
            spawnerSystem.BindVehiclesPrefabs(spawner.carsPrefabs);
            pavementPathfindingSystem.BindNodeData(pulledNodes);
            vehicleMovementSystem.BindRoadConnectionsData(pulledRoadConnections);

            //randomSystem.Enabled = true;
            spawnerSystem.Enabled = true;
            pavementPathfindingSystem.Enabled = true;
            citizenMovementSystem.Enabled = true;
            vehicleMovementSystem.Enabled = true;
        }

        private void DisableDOTSSystems()
        {
            World world = World.DefaultGameObjectInjectionWorld;
            //var randomSystem = world.GetExistingSystem<RandomSystem>();
            var spawnerSystem = world.GetExistingSystem<SpawnerSystem>();
            var pavementPathfindingSystem = world.GetExistingSystem<PavementPathfindingSystem>();
            var citizenMovementSystem = world.GetExistingSystem<CitizenMovementSystem>();
            var vehicleMovementSystem = world.GetExistingSystem<VehicleMovementSystem>();

            spawnerSystem.Enabled = false;
            pavementPathfindingSystem.Enabled = false;
            citizenMovementSystem.Enabled = false;
            vehicleMovementSystem.Enabled = false;
        }
    }
}