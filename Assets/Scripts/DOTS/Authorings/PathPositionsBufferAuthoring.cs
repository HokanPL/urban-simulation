﻿using Unity.Entities;
using UnityEngine;

namespace UrbanWorld
{
    public class PathPositionsBufferAuthoring : MonoBehaviour, IConvertGameObjectToEntity
    {
        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddBuffer<PathPositionsBuffer>(entity);
        }
    }
}
