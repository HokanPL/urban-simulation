using System.Linq;
using Unity.Collections;
using Unity.Entities;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Mathematics.Extensions;
using Unity.Transforms;

namespace UrbanWorld.Pathfinding
{
    [DisableAutoCreation]
    public class PavementPathfindingSystem : SystemBase
    {
        private NodeDOTS[] nodes;
        private NodeDOTS[] nodesWalkable;
        private int2 gridSize;

        public void BindNodeData(NodeOOP[,] nodesOriginal)
        {
            gridSize.x = nodesOriginal.GetLength(0);
            gridSize.y = nodesOriginal.GetLength(1);

            nodes = new NodeDOTS[gridSize.x * gridSize.y];

            for (int i = 0; i < gridSize.x; i++)
            {
                for (int j = 0; j < gridSize.y; j++)
                {
                    int nodeId = CalculateId(j, i, gridSize.x);

                    nodes[nodeId] = new NodeDOTS
                    {
                        id = nodeId,
                        coords = new int2(j, i),
                        position = nodesOriginal[i, j].position,
                        gCost = int.MaxValue,
                        hCost = int.MaxValue,
                        parentNodeId = -1
                    };
                }
            }

            nodesWalkable = nodes.Select(n => n).Where(n => n.isWalkable).ToArray();
            
        }

        protected override void OnUpdate()
        {
            var randomArray = World.GetExistingSystem<RandomizerSystem>().RandomArray;

            int2 gridSizeLocal = gridSize;
            NativeArray<NodeDOTS> nodesArrayLocal = new NativeArray<NodeDOTS>(nodes, Allocator.TempJob);
            NativeArray<NodeDOTS> nodesWalkableLocal = new NativeArray<NodeDOTS>(nodesWalkable, Allocator.TempJob);


            //var commandBuffer = m_EntityCommandBufferSystem.CreateCommandBuffer();

            Entities
                //.WithNativeDisableParallelForRestriction(pathFollowFromEntity)
                .WithAll<CitizenComponent>()
                .ForEach((
                    int entityInQueryIndex,
                    DynamicBuffer<PathPositionsBuffer> pathPositionsBuffer,
                    ref PathFollowComponent pathFollow,
                    in int nativeThreadIndex,
                    in Translation position) =>
                    {
                        if (pathFollow.pathIndex == -1)
                        {
                            var radom = randomArray[nativeThreadIndex];
                            int myRandomId = radom.NextInt(0, nodesWalkableLocal.Length);
                            randomArray[nativeThreadIndex] = radom;

                            float3 startPosition = position.Value;
                            int2 endCoords = nodesWalkableLocal[myRandomId].coords;
                            int2 gridSize = gridSizeLocal;
                            NativeArray<NodeDOTS> nodesArray = new NativeArray<NodeDOTS>(nodesArrayLocal, Allocator.Temp);

                            int2 startCoords = new int2(-1, -1);
                            float minDistance = float.MaxValue;
                            for (int i = 0; i < nodesArray.Length; i++)
                            {
                                float currentDistance = math.distance(startPosition, nodesArray[i].position);
                                if (currentDistance < minDistance)
                                {
                                    startCoords = nodesArray[i].coords;
                                    minDistance = currentDistance;
                                }
                            }

                            int startId = startCoords.x + startCoords.y * gridSize.x;
                            NodeDOTS startNode = nodesArray[startId];
                            startNode.gCost = 0;

                            int endId = endCoords.x + endCoords.y * gridSize.x;
                            NodeDOTS endNode = nodesArray[endId];

                            if (startId == endId)
                            {
                                pathPositionsBuffer.Clear();
                                pathPositionsBuffer.Add(endNode.position);

                                pathFollow.pathIndex = 0;
                            }
                            else
                            {
                                nodesArray[startNode.id] = startNode;

                                NativeList<int> openedList = new NativeList<int>(Allocator.Temp);
                                NativeList<int> closedList = new NativeList<int>(Allocator.Temp);

                                openedList.Add(startNode.id);

                                NativeArray<int2> neighbourOffsetArray = new NativeArray<int2>(4, Allocator.Temp);
                                neighbourOffsetArray[0] = new int2(-1, 0);
                                neighbourOffsetArray[1] = new int2(1, 0);
                                neighbourOffsetArray[2] = new int2(0, -1);
                                neighbourOffsetArray[3] = new int2(0, 1);

                                while (openedList.Length > 0)
                                {
                                    NodeDOTS lowestCostNode = nodesArray[openedList[0]];
                                    for (int i = 1; i < openedList.Length; i++)
                                    {
                                        NodeDOTS node = nodesArray[openedList[i]];
                                        if (node.fCost < lowestCostNode.fCost)
                                        {
                                            lowestCostNode = node;
                                        }
                                    }
                                    int currentNodeId = lowestCostNode.id;
                                    NodeDOTS currentNode = nodesArray[currentNodeId];
                                    if (currentNodeId == endId)
                                    {
                                        //reached targer
                                        break;
                                    }

                                    for (int i = 0; i < openedList.Length; i++)
                                    {
                                        if (openedList[i] == currentNodeId)
                                        {
                                            openedList.RemoveAtSwapBack(i);
                                            break;
                                        }
                                    }
                                    closedList.Add(currentNodeId);

                                    for (int i = 0; i < neighbourOffsetArray.Length; i++)
                                    {
                                        int2 neighbourPosition = currentNode.coords + neighbourOffsetArray[i];
                                        bool isPositionOutsideGrid = neighbourPosition.x < 0 || neighbourPosition.x >= gridSize.x || neighbourPosition.y < 0 || neighbourPosition.y >= gridSize.y;
                                        if (isPositionOutsideGrid)
                                        {
                                            continue;
                                        }

                                        int neighbourNodeId = neighbourPosition.x + neighbourPosition.y * gridSize.x;

                                        if (closedList.Contains(neighbourNodeId))
                                            continue;

                                        NodeDOTS neighbourNode = nodesArray[neighbourNodeId];
                                        if (!neighbourNode.isWalkable)
                                            continue;

                                        int tentativeGCost = currentNode.gCost + 1;
                                        if (tentativeGCost < neighbourNode.gCost)
                                        {
                                            neighbourNode.parentNodeId = currentNodeId;
                                            neighbourNode.gCost = tentativeGCost;
                                            neighbourNode.hCost = math.abs(neighbourPosition.x - endCoords.x) + math.abs(neighbourPosition.y - endCoords.y);
                                            nodesArray[neighbourNodeId] = neighbourNode;

                                            if (!openedList.Contains(neighbourNodeId))
                                            {
                                                openedList.Add(neighbourNodeId);
                                            }
                                        }
                                    }
                                }

                                pathPositionsBuffer.Clear();

                                NodeDOTS pathNode = nodesArray[endId];
                                if (pathNode.parentNodeId != -1)
                                    pathPositionsBuffer.Add(pathNode.position);

                                float3 dir = new float3(-1, -1, -1);
                                while (pathNode.parentNodeId != -1)
                                {
                                    NodeDOTS parentNode = nodesArray[pathNode.parentNodeId];
                                    pathPositionsBuffer.Add(parentNode.position);
                                    pathNode = parentNode;
                                }

                                pathFollow.pathIndex = pathPositionsBuffer.Length - 1;

                                openedList.Dispose();
                                closedList.Dispose();
                                neighbourOffsetArray.Dispose();
                            }
                            nodesArray.Dispose();
                        }
                    }).Schedule();

            Dependency.Complete();

            nodesArrayLocal.Dispose();
            nodesWalkableLocal.Dispose();
        }

        private int CalculateId(int x, int y, int gridWidth)
        {
            return x + y * gridWidth;
        }
    }

    public struct NodeDOTS
    {
        public int id;
        public int2 coords;
        public float3 position;

        public int gCost;
        public int hCost;
        public int fCost => (gCost + hCost > int.MaxValue) ? int.MaxValue : gCost + hCost;

        public bool isWalkable => position.y >= 0;

        public int parentNodeId;
    }
}