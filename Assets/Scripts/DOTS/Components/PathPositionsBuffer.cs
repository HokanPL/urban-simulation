﻿using Unity.Entities;
using Unity.Mathematics;

namespace UrbanWorld
{
    [InternalBufferCapacity(100)]
    public struct PathPositionsBuffer : IBufferElementData
    {
        public float3 position;
        
        public static implicit operator PathPositionsBuffer(float3 addedValue)
        {
            return new PathPositionsBuffer { position = addedValue };
        }
        public static implicit operator float3(PathPositionsBuffer element) { return element.position; }
    }
}
