﻿using Unity.Entities;
using Unity.Mathematics;

namespace UrbanWorld.Pathfinding
{
    public struct PathfindingParametersComponent : IComponentData
    {
        public float3 startPosition;
        public float3 endPosition;
    }
}