using Unity.Entities;
using Unity.Mathematics;

//tested - 8 is maximum
[InternalBufferCapacity(8)]
public struct CitizenWalkableCoordsBuffer : IBufferElementData
{
    public float3 walkableCoords;

    public static implicit operator CitizenWalkableCoordsBuffer(float3 addedValue)
    {
        return new CitizenWalkableCoordsBuffer { walkableCoords = addedValue };
    }

    public static implicit operator float3(CitizenWalkableCoordsBuffer element) { return element.walkableCoords; }
}
