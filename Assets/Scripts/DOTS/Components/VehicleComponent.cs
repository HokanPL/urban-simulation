using Unity.Entities;
using Unity.Mathematics;

namespace UrbanWorld
{
    public struct VehicleComponent : IComponentData
    {
        public float speed;
        public float3 target;
    }
}
