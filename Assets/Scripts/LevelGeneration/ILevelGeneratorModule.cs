﻿using System.Collections;
using UrbanWorld;

namespace UrbanWorld.Generators
{
    public interface ILevelGeneratorModule
    {
        void Generate(Cell[,] nodes);
    }

}