using UnityEngine;
using System.Linq;
using UrbanWorld.Pathfinding;
using Random = UnityEngine.Random;

namespace UrbanWorld.Simulation
{
    public class GameObjectsSpawner : MonoBehaviour
    {
        public GameObject[] citizensPrefabs;
        public GameObject[] carsPrefabs;

        public PavementNetwork citizensNetwork;
        public RoadNetwork vehiclesNetwork;

        public Transform spawnedCitizensHolder;
        public Transform spawnedVehiclesHolder;

        private int citizensPrefabsLength;
        private int carsPrefabsLength;

        private int citizensTotal;
        private int vehiclesTotal;

        private void Awake()
        {
            citizensPrefabsLength = citizensPrefabs.Length;
            carsPrefabsLength = carsPrefabs.Length;
        }

        public void StartSpawning()
        {
            StopSpawning();

            var residentalBuildings = FindObjectsOfType<ResidentalBuilding>();

            citizensTotal = residentalBuildings.Sum(t => t.citizensAmount) / 10;
            vehiclesTotal = residentalBuildings.Select(t => t).Where(t => t.vehicleEntrance != null).Count() * 3;

            CitizenSpawner();
            VehicleSpawner();
        }

        public void StopSpawning()
        {
            foreach (Transform t in spawnedCitizensHolder)
                Destroy(t.gameObject);

            foreach (Transform t in spawnedVehiclesHolder)
                Destroy(t.gameObject);
        }

        private void CitizenSpawner()
        {
            for (int i = 0; i < citizensTotal; i++)
            {
                Instantiate(GetRandomCitizenPrefab(), citizensNetwork.GetRandomNode().position, Quaternion.identity, spawnedCitizensHolder)/*)*/;
            }
        }

        private void VehicleSpawner()
        {
            for (int i = 0; i < vehiclesTotal; i++)
            {
                Instantiate(GetRandomVehiclePrefab(), vehiclesNetwork.GetRandomPosition(), Quaternion.identity, spawnedVehiclesHolder);
            }
        }

        private GameObject GetRandomCitizenPrefab()
        {
            return citizensPrefabs[Random.Range(0, citizensPrefabsLength)];
        }

        private GameObject GetRandomVehiclePrefab()
        {
            return carsPrefabs[Random.Range(0, carsPrefabsLength)];
        }
    }
}