﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace UrbanWorld.Generators
{
    public class ContentGenerator : ILevelGeneratorModule
    {
        private Building[] placeableBuildings;
        private RoadVariantsSet[,,,] placeableRoads;
        private ContentPrefabsContainer prefabsContainer;

        public ContentGenerator(ContentPrefabsContainer container)
        {
            prefabsContainer = container;
        }

        private void PullPrefabsFromContainer()
        {
            placeableBuildings = prefabsContainer.placeableBuildings;
            placeableRoads = new RoadVariantsSet[2, 2, 2, 2];

            foreach (var road in prefabsContainer.placeableRoads)
            {
                if (road == null) continue;
                int roadNorth = (road.name.Contains("N")) ? 1 : 0;
                int roadSouth = (road.name.Contains("S")) ? 1 : 0;
                int roadWest = (road.name.Contains("W")) ? 1 : 0;
                int roadEast = (road.name.Contains("E")) ? 1 : 0;

                if (placeableRoads[roadNorth, roadEast, roadSouth, roadWest] == null)
                    placeableRoads[roadNorth, roadEast, roadSouth, roadWest] = new RoadVariantsSet();

                placeableRoads[roadNorth, roadEast, roadSouth, roadWest].AddVariant(road);
            }
        }

        void ILevelGeneratorModule.Generate(Cell[,] nodes)
        {
            PullPrefabsFromContainer();

            uint gridSizeX = (byte)nodes.GetLength(0);
            uint gridSizeZ = (byte)nodes.GetLength(1);

            for (int i = 0; i < gridSizeX; i++)
            {
                for (int j = 0; j < gridSizeZ; j++)
                {
                    if (nodes[i, j].IsOccupied()) continue;


                    switch (nodes[i, j].type)
                    {
                        case Cell.NodeType.building:

                            int ii = i + 1;
                            int jj = j + 1;

                            for (; ii < gridSizeX; ii++)
                                if (nodes[ii, j].type == Cell.NodeType.road || nodes[ii, j].IsOccupied())
                                    break;

                            for (; jj < gridSizeZ; jj++)
                                if (nodes[i, jj].type == Cell.NodeType.road || nodes[i, jj].IsOccupied())
                                    break;

                            int maxSizeX = ii - i;
                            int maxSizeZ = jj - j;

                            //choose objects which can be put because of its' size
                            var possiblePlaceables = placeableBuildings.Select(t => t).Where(t => t.CanBeInside(maxSizeX, maxSizeZ)).ToList();


                            for (int bId = possiblePlaceables.Count - 1; bId >= 0 ; bId--)
                            {
                                if (possiblePlaceables[bId].name.Contains("east"))
                                {
                                    int roadNodeCoordX = i + possiblePlaceables[bId].GetSizeX();

                                    if (roadNodeCoordX == gridSizeX || nodes[roadNodeCoordX, j].type != Cell.NodeType.road)
                                        possiblePlaceables.RemoveAt(bId);
                                }
                                else if (possiblePlaceables[bId].name.Contains("west"))
                                {
                                    int roadNodeCoordX = i - 1;

                                    if (roadNodeCoordX < 0 || nodes[roadNodeCoordX, j].type != Cell.NodeType.road)
                                        possiblePlaceables.RemoveAt(bId);
                                }
                                else if (possiblePlaceables[bId].name.Contains("north"))
                                {
                                    int roadNodeCoordZ = j + possiblePlaceables[bId].GetSizeZ();

                                    if (roadNodeCoordZ == gridSizeZ || nodes[i, roadNodeCoordZ].type != Cell.NodeType.road)
                                        possiblePlaceables.RemoveAt(bId);
                                }
                                else if (possiblePlaceables[bId].name.Contains("south"))
                                {
                                    int roadNodeCoordZ = j - 1;

                                    if (roadNodeCoordZ < 0 || nodes[i, roadNodeCoordZ].type != Cell.NodeType.road)
                                        possiblePlaceables.RemoveAt(bId);
                                }
                            }

                            if (possiblePlaceables.Count == 0) break;

                            Building chosenBuilding = possiblePlaceables[Random.Range(0, possiblePlaceables.Count)];
                            string randomContent = chosenBuilding.name;

                            for (int x = i; x < i + chosenBuilding.GetSizeX(); x++)
                                for (int z = j; z < j + chosenBuilding.GetSizeZ(); z++)
                                    nodes[x, z].Occupy();

                            //only pivot node needs to have additional info what exactly is placed
                            nodes[i, j].OccupyWith(randomContent);

                            break;

                            //check if directions are relative to world - testing on runtime required
                        case Cell.NodeType.road:


                            int isRoadSouth = (j - 1 < 0 || nodes[i, j - 1].type != Cell.NodeType.road) ? 0 : 1;
                            int isRoadNorth = (j + 1 == gridSizeZ || nodes[i, j + 1].type != Cell.NodeType.road) ? 0 : 1;
                            int isRoadWest = (i - 1 < 0 || nodes[i - 1, j].type != Cell.NodeType.road) ? 0 : 1;
                            int isRoadEast = (i + 1 == gridSizeX || nodes[i + 1, j].type != Cell.NodeType.road) ? 0 : 1;

                            nodes[i, j].OccupyWith(placeableRoads[isRoadNorth, isRoadEast, isRoadSouth, isRoadWest].GetRandomVariant().name);

                            break;


                        case Cell.NodeType._undefined:
                            throw new System.Exception("Grid invalid - there's at least 1 node of [_undefined] type");
                    }

                }
            }
        }
    }

}