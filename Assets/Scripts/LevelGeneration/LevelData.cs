﻿using UrbanWorld;

namespace SaveLoadManagement
{
    public class LevelData
    {
        public int sizeX;
        public int sizeZ;

        public Cell[] nodes;
        public LevelData(Cell[,] nodes)
        {
            sizeX = nodes.GetLength(0);
            sizeZ = nodes.GetLength(1);

            this.nodes = new Cell[sizeX * sizeZ];

            int id = 0;
            foreach (var n in nodes)
            {
                this.nodes[id] = n;
                id++;
            }
        }

        public Cell[,] GetNodes()
        {
            Cell[,] result = new Cell[sizeX, sizeZ];

            for (int i = 0; i < nodes.Length; i++)
            {
                result[nodes[i].nodePositionX, nodes[i].nodePositionZ] = nodes[i];
            }

            return result;
        }
    }
}