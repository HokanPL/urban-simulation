﻿using System;
using System.Collections.Generic;
using System.Linq;
using Unity.Mathematics;
using UnityEngine;
using UrbanWorld.Pathfinding;

namespace UrbanWorld.Generators
{
    public class NetworkGenerator : MonoBehaviour
    {
        [SerializeField] private RoadNetwork roadNetwork;
        [SerializeField] private PavementNetwork pavementNetwork;

        public void Generate()
        {
            roadNetwork.CreateNetwork();
            pavementNetwork.CreateNetwork();
        }

        public void Clear()
        {
            roadNetwork.ClearNetwork();
            pavementNetwork.ClearNetwork();
        }

        public NodeOOP[,] PullNodes() => pavementNetwork.nodes;
        
        public Vector3[] PullVehicleStartLocationsAsVectors() => roadNetwork.trafficRoutes.Select(r => r.startPosition).ToArray();
        public float3[] PullVehicleStartLocationsAsFloat3s() => roadNetwork.trafficRoutes.Select(r => (float3)r.startPosition).ToArray();
        public HashSet<RoadNetwork.TrafficRoute> PullRoutes() => roadNetwork.trafficRoutes;

        public float3[] PullCitizenStartLocationsAsFloat3s()
        {
            HashSet<float3> result = new HashSet<float3>();
            foreach(var n in pavementNetwork.nodes)
            {
                if (n.walkable)
                    result.Add(n.position);
            }
            return result.ToArray();
        }

        private void OnDrawGizmosSelected()
        {
            float3 up = new float3(0, 0.05f, 0);
            Vector3 upp = new Vector3(0, 0.05f, 0);

            if (roadNetwork != null)
            {
                Gizmos.color = Color.red;
                var routes = roadNetwork.trafficRoutes;

                foreach (var r in routes)
                {
                    Gizmos.DrawLine(r.startPosition + upp, r.endPosition + upp);
                    Gizmos.DrawCube(r.startPosition, new Vector3(0.1f, 0.1f, 0.1f));
                    //Gizmos.DrawCube(r.endPosition, new Vector3(0.1f, 0.1f, 0.1f));
                }
            }
            if (pavementNetwork != null)
            {
                Gizmos.color = Color.yellow;
                var nodes = pavementNetwork.nodes;

                if (nodes == null)
                    return;

                int sizeX = nodes.GetLength(0);
                int sizeZ = nodes.GetLength(1);

                for (int i = 0; i < sizeX; i++)
                {
                    for (int j = 0; j < sizeZ; j++)
                    {
                        if (i + 1 != sizeX)
                        {
                            NodeOOP n1 = nodes[i, j];
                            NodeOOP n2 = nodes[i + 1, j];
                            if (n1.walkable && n2.walkable)
                                Gizmos.DrawLine(n1.position + upp, n2.position + upp);
                        }
                        if (j + 1 != sizeZ)
                        {
                            NodeOOP n1 = nodes[i, j];
                            NodeOOP n2 = nodes[i, j + 1];
                            if (n1.walkable && n2.walkable)
                                Gizmos.DrawLine(n1.position + upp, n2.position + upp);
                        }
                    }
                }
            }
        }
    }
}