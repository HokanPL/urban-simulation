﻿using UnityEngine;

namespace UrbanWorld.Generators
{
    public class BaseGenerator : ILevelGeneratorModule
    {
        void ILevelGeneratorModule.Generate(Cell[,] nodes)
        {
            uint gridSizeX = (byte)nodes.GetLength(0);
            uint gridSizeZ = (byte)nodes.GetLength(1);

            for (int i = 0; i < gridSizeX; i++)
                for (int j = 0; j < gridSizeZ; j++)
                    nodes[i, j] = new Cell(i, j);

            for (int xMin = 0; xMin < gridSizeX; xMin++)
            {
                for (int zMin = 0; zMin < gridSizeZ; zMin++)
                {
                    if (nodes[xMin, zMin].type != Cell.NodeType._undefined)
                        continue;

                    int xRectangleSize = Random.Range(1, 3) * 2;
                    int zRectangleSize = Random.Range(1, 3) * 2;

                    var xMax = Mathf.Min(xMin + xRectangleSize, gridSizeX - 1);
                    var zMax = Mathf.Min(zMin + zRectangleSize, gridSizeZ - 1);

                    for (int i = xMin - 1; i <= xMax + 1; i++)
                    {
                        for (int j = zMin - 1; j <= zMax + 1; j++)
                        {
                            if (i < 0 || j < 0 || i >= gridSizeX || j >= gridSizeZ) continue;

                            if (nodes[i, j].type == Cell.NodeType._undefined)
                            {
                                if (i == xMin - 1 || i == xMax + 1 || j == zMin - 1 || j == zMax + 1)
                                {
                                    nodes[i, j].type = Cell.NodeType.road;
                                }
                                else
                                {
                                    nodes[i, j].type = Cell.NodeType.building;
                                }
                            }
                        }
                    }
                }
            }

            for (int i = 0; i < gridSizeX; i++)
            {
                for (int j = 0; j < gridSizeZ; j++)
                {
                    nodes[i, j].SetRoadNeighbour(WorldDirection.west, !(i - 1 < 0 || nodes[i - 1, j].type != Cell.NodeType.road));
                    nodes[i, j].SetRoadNeighbour(WorldDirection.east, !(i + 1 == gridSizeX || nodes[i + 1, j].type != Cell.NodeType.road));
                    nodes[i, j].SetRoadNeighbour(WorldDirection.south, !(j - 1 < 0 || nodes[i, j - 1].type != Cell.NodeType.road));
                    nodes[i, j].SetRoadNeighbour(WorldDirection.north, !(j + 1 == gridSizeZ || nodes[i, j + 1].type != Cell.NodeType.road));
                }
            }
        }
    }

}