using System;
using Unity.Mathematics;
using UnityEngine;

namespace UrbanWorld
{
    [Serializable]
    public struct Path
    {
        public Connector connectorOut;
    }

    public enum ConnectorType { In, Out };


    [Serializable]
    public class Connector : MonoBehaviour
    {
        public ConnectorType type;
        public WorldDirection moveDirection;
        public Path[] paths;
        public bool belongsToStraightRoad = false;
    }

    public struct RoadConnectionData
    {
        public float3 startPosition;
        public float3 endPosition;
    }
}