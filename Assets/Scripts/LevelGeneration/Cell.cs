﻿namespace UrbanWorld
{
    public enum WorldDirection { north, south, east, west }

    [System.Serializable]
    public class Cell
    {
        public int nodePositionX;
        public int nodePositionZ;

        public enum NodeType { _undefined, road, building }

        [System.NonSerialized] public NodeType type = NodeType._undefined;
        [System.NonSerialized] private bool isOccupied = false;

        public bool HasRoadNeighbourNorth { get; private set; } = false;
        public bool HasRoadNeighbourSouth { get; private set; } = false;
        public bool HasRoadNeighbourEast { get; private set; } = false;
        public bool HasRoadNeighbourWest { get; private set; } = false;

        public string placeableName = string.Empty;

        public Cell(int x, int z)
        {
            nodePositionX = x;
            nodePositionZ = z;
        }

        public bool IsOccupied() => isOccupied;

        public void Occupy() => isOccupied = true;
        public void OccupyWith(string contentName)
        {
            placeableName = contentName;
            isOccupied = true;
        }

        public void SetRoadNeighbour(WorldDirection direction, bool hasNeighbour)
        {
            switch (direction)
            {
                case WorldDirection.north:
                    HasRoadNeighbourNorth = hasNeighbour; break;
                case WorldDirection.south:
                    HasRoadNeighbourSouth = hasNeighbour; break;
                case WorldDirection.east:
                    HasRoadNeighbourEast = hasNeighbour; break;
                case WorldDirection.west:
                    HasRoadNeighbourWest = hasNeighbour; break;
            }
        }
    }
}