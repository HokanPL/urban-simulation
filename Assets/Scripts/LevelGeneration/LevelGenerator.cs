using AppliacationManagement;
using System.Collections;
using UnityEngine;

namespace UrbanWorld.Generators
{
    public class LevelGenerator : MonoBehaviour
    {
        [SerializeField] private ApplicationManager mediator;
        [SerializeField] private ContentPrefabsContainer prefabsContainer;
        [SerializeField] private LevelVisualizer visualizer;

        private ILevelGeneratorModule[] generators;

        private Cell[,] cells;

        private void Awake()
        {
            cells = new Cell[0, 0];
            generators = new ILevelGeneratorModule[2];
            generators[0] = new BaseGenerator();
            generators[1] = new ContentGenerator(prefabsContainer);
        }

        public void Generate(string filename, uint sizeX, uint sizeZ)
        {
            ClearGeneratedData();
            cells = new Cell[sizeX, sizeZ];

            for (int i = 0; i < generators.Length; i++)
            {
                generators[i].Generate(cells);
            }

            mediator.PushCells(cells, filename, false);
        }

        public void ClearGeneratedData()
        {
            visualizer.HideUrbanWorld();
            cells = new Cell[0, 0];
            mediator.PushCells(cells, "", false);
        }
    }
}