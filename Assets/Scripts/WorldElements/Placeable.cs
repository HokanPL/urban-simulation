﻿using UnityEngine;

namespace UrbanWorld
{
    [DisallowMultipleComponent]
    public class Placeable : MonoBehaviour
    {
        [SerializeField] [Min(1)] protected int sizeX = 1;
        [SerializeField] [Min(1)] protected int sizeY = 1;

        public virtual Vector3[] GetWalkableCoords() => null;

        public int GetSizeX() => sizeX;
        public int GetSizeZ() => sizeY;
        public bool CanBeInside(int sizeX, int sizeY) => (this.sizeX <= sizeX && this.sizeY <= sizeY);
    }
}