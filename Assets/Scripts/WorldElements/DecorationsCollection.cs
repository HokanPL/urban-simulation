﻿using UnityEngine;

public abstract class DecorationsCollection : MonoBehaviour
{
    [SerializeField] protected DecorationSet[] decorationSet;
    public virtual void ShowRandomDecorations() { }
}
