using UnityEngine;
using Unity.Entities;
using UrbanWorld.Pathfinding;

namespace UrbanWorld.Simulation
{
    public class Citizen : MonoBehaviour, IUpdateable, IConvertGameObjectToEntity
    {
        public float speed = 3f;
        private Vector3 currentTarget;
        private int currentTargetId = 0;

        private Vector3[] myPath = new Vector3[0];

        private void Start()
        {
            SelectNextTarget();
        }

        private void OnEnable()
        {
            ApplicationPlayerLoop.Instance.AddUpdateable(this);
        }

        private void OnDisable()
        {
            ApplicationPlayerLoop.Instance.RemoveUpdateable(this);
        }

        void IUpdateable.UpdateMe()
        {
            if (transform.position == currentTarget)
            {
                currentTargetId++;
            }

            if (myPath.Length <= currentTargetId)
            {
                SelectNextTarget();
                return;
            }

            currentTarget = myPath[currentTargetId];

            transform.position = Vector3.MoveTowards(transform.position, currentTarget, Time.deltaTime * speed);
            transform.LookAt(currentTarget);
        }
        
        private void SelectNextTarget()
        {
            Vector3 target = PavementPathfinding.Instance.GetRandomTarget();
            myPath = PavementPathfinding.Instance.CalculatePath(transform.position, target);
            currentTargetId = 0;
        }

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            dstManager.AddComponentData(entity, new CitizenComponent { speed = speed });
        }
    }
}