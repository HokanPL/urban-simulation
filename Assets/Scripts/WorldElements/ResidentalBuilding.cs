using Unity.Entities;
using UnityEngine;
using UrbanWorld.Generators;
using Unity.Mathematics;

namespace UrbanWorld
{
    public class ResidentalBuilding : Building, IConvertGameObjectToEntity
    {
        [Min(0)] public int citizensAmount;

        private void Start()
        {
            GetWalkableCoords();
        }

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            bool hasVehicleEntrance = (vehicleEntrance != null);

            var spawnerData = new SpawnerComponent
            {
                citizensToSpawn = citizensAmount,
                vehiclesToSpawn = (hasVehicleEntrance) ? 1 : 0
            };
            dstManager.AddComponentData(entity, spawnerData);
            /*
            DynamicBuffer<CitizenWalkableCoordsBuffer> citizenWalkableCoordsBuffer = dstManager.AddBuffer<CitizenWalkableCoordsBuffer>(entity);
            var coordsToAdd = GetWalkableCoords();
            for (int i = 0; i < coordsToAdd.Length; i++)
                citizenWalkableCoordsBuffer.Add((float3)coordsToAdd[i]);
                //citizenWalkableCoordsBuffer.Add(new CitizenWalkableCoordsBuffer { walkableCoords = coordsToAdd[i] });
            */
        }
    }
}