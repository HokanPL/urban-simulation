using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace UrbanWorld
{
    public class Building : Placeable
    {
        [Header("Data for Citizen Entrance Component")]
        public GameObject groundDependentHolder; //if null return;
        public DecorationsCollection[] optionalDecorationsSets;

        public Transform citizenEntrance;
        public Transform vehicleEntrance;

        public override Vector3[] GetWalkableCoords()
        {
            Vector3 startCoords = transform.position + new Vector3(-0.5f, 0, -0.5f);
            Vector3 currentCoords = startCoords;

            var result = new HashSet<Vector3>();
            result.Add(currentCoords);

            for (int i = 0; i < sizeX * 2; i++)
            {
                currentCoords.x += 0.5f;
                result.Add(currentCoords);
            }

            for (int i = 0; i < sizeY * 2; i++)
            {
                currentCoords.z += 0.5f;
                result.Add(currentCoords);
            }

            for (int i = 0; i < sizeX * 2; i++)
            {
                currentCoords.x -= 0.5f;
                result.Add(currentCoords);
            }

            for (int i = 0; i < sizeY * 2; i++)
            {
                currentCoords.z -= 0.5f;
                result.Add(currentCoords);
            }

            result.Add(citizenEntrance.position);

            return result.ToArray();
        }

        public void RandomizeDecorations()
        {
            if (optionalDecorationsSets == null) return;

            for (int i = 0; i < optionalDecorationsSets.Length; i++)
                optionalDecorationsSets[i].ShowRandomDecorations();
        }
    }
}