using UnityEngine;
using Unity.Entities;
using UrbanWorld.Pathfinding;
using UrbanWorld.Simulation;

namespace UrbanWorld
{
    public class Vehicle : MonoBehaviour, IUpdateable, IConvertGameObjectToEntity
    {
        public float speed = 4f;
        private Vector3 target;

        private void Start()
        {
            SelectNextTarget();
        }

        private void OnEnable()
        {
            ApplicationPlayerLoop.Instance.AddUpdateable(this);
        }

        private void OnDisable()
        {
            ApplicationPlayerLoop.Instance.RemoveUpdateable(this);
        }

        void IUpdateable.UpdateMe()
        {
            if (target == Vector3.zero) return;

            if (transform.position == target)
                SelectNextTarget();

            transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * speed);
            transform.LookAt(target);
        }

        private void SelectNextTarget()
        {
            target = RoadPathfinding.Instance.GetNextTarget(transform.position);
        }

        public void Convert(Entity entity, EntityManager dstManager, GameObjectConversionSystem conversionSystem)
        {
            var vehicle = new VehicleComponent
            {
                speed = speed,
                target = target
            };
            dstManager.AddComponentData(entity, vehicle);
        }
    }
}